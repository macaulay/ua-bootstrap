#!/usr/bin/env bash
sed -i "s#http://localhost:8888#${S3URL}/review/${BRANCH}#g" $BITBUCKET_CLONE_DIR/backstop.json
sed -i "s#http://uadigital.arizona.edu/ua-bootstrap#${S3URL}/review/master#g" $BITBUCKET_CLONE_DIR/backstop.json
npm run backstop_reference
npm run backstop_test