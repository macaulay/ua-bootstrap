#!/usr/bin/env bash
. ./.build-tools/semver.sh
latest=$(curl -s http://cdn.uadigital.arizona.edu/lib/ua-bootstrap/latest/release)
current=$(git tag | tail -n1)
semverGT $current $latest
if [ $? -ne 1 ]; then
    pwd
    aws s3 rm s3://${AWS_S3_BUCKET}/lib/ua-bootstrap/latest/ --recursive
    echo $current > release
    aws s3 cp release  s3://${AWS_S3_BUCKET}/lib/ua-bootstrap/latest/
    aws s3 sync dist/css/ s3://${AWS_S3_BUCKET}/lib/ua-bootstrap/$current/
    aws s3 sync dist/css/ s3://${AWS_S3_BUCKET}/lib/ua-bootstrap/latest/
fi

